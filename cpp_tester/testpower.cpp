#include <iostream>
#include "testpower.h"
#include "classwewanttotest.h"


PowerTest::PowerTest(const std::string &test_name):
    TestObject (test_name)
{

}

bool PowerTest::test() const
{
    using namespace std;
    const vector<vector<double>> test_data = {
        {1, 2, 1},
        {2, 3, 8},
        {5, 1, 20}, // introduce an intended error (5^1 != 20)
        {2, -1, 0.5},
        {100, -1, 0.01},
        {10, 0.5, 3.16227766016837933},
        {100, 0.01, 1.0471285480508995334645020315281}
    } ;

    cout << m_testName << " started... \n";


    for(auto const& value : test_data)
    {
        if(singleTest(value[0], value[1], value[2]) == false)
            return false;
    }
    return true;
}

bool PowerTest::singleTest(double x, double y, double reference_value) const
{
    using namespace std;
    const double double_epsilon = 0.000000001;

    ClassWeWantToTest examined_class;
    double calculated = examined_class.power(x, y);


    if(abs(calculated - reference_value) < double_epsilon)
        return  true;
    else
    {
        cout << "FAILED: x: " << to_string(x) << "; " <<
                          "y: "  << to_string(y) << "; " <<
                          "expected: " << to_string(reference_value) << "; " <<
                          "received: " << to_string(calculated) << endl;
        return false;
    }


}
