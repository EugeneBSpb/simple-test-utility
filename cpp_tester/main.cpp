/**
 * Description:  Usage example of tester utility.
 *
 *
 **/


#include <iostream>
#include "../common_tester_files/tester.h"
#include "testsum.h"
#include "testpower.h"


int main()
{
    using namespace std;

        // Here we are creating the object of type Tester
    Tester test_manager;

        // Creation of our custom tests implementation
    unique_ptr<SumTest> sum_test(new SumTest("Sum test"));
    unique_ptr<PowerTest> power_test(new PowerTest("Power test"));

        // Adding tests to queue and perfoming tests
    test_manager.addTestObject(move(sum_test));
    test_manager.addTestObject(move(power_test));
    test_manager.performTests();

    return 0;
}
