#ifndef CLASSWEWANTTOTEST_H
#define CLASSWEWANTTOTEST_H

#include <cmath>

class ClassWeWantToTest
{
    friend class SumTest;
    friend class PowerTest;
public:
    ClassWeWantToTest();
private:
    int sum(int x, int y) {return (x+y);}
    double power(double x, double power){return pow(x, power);}
};


#endif // CLASSWEWANTTOTEST_H
