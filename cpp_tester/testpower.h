#ifndef TESTPOWER_H
#define TESTPOWER_H

#include "../common_tester_files/tester.h"


class PowerTest : public TestObject
{
public:
    PowerTest(const std::string &test_name);
    virtual bool test() const override;
    bool singleTest(double x, double y, double reference_value) const;
};

#endif // TESTPOWER_H
