TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        main.cpp \
    classwewanttotest.cpp \
    testsum.cpp \
    testpower.cpp \
    ../common_tester_files/tester.cpp

HEADERS += \
    classwewanttotest.h \
    testsum.h \
    testpower.h \
    ../common_tester_files/tester.h
