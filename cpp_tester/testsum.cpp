#include "testsum.h"
#include <iostream>
#include <utility>
#include "classwewanttotest.h"


SumTest::SumTest(const std::string &test_name):
    TestObject (test_name)
{

}

bool SumTest::test() const
{
    using namespace std;
    const vector<vector<int>> test_data = {
        {1,2,3},
        {2,3,5},
        {5,1,6},
        {18,22,40},
        {45,3,48},
        {2,11,13},
        {4,1200,1204},
        {10000,100033,110033},
        {-2,-55,-57},
        {-18,100,82},
        {-10000000,50000,-9950000},
        {15,-20,-5},
        {-1,1,0},
        {50,-50,0}
    } ;

    cout << m_testName << " started... \n";

    for(auto const& value : test_data)
    {
        if(singleTest(value[0], value[1], value[2]) == false)
            return false;
    }



    return true;
}

bool SumTest::singleTest(int x, int y, int reference_value) const
{
    using namespace std;

    ClassWeWantToTest examined_class;

    int calculated = examined_class.sum(x, y);


    if(calculated == reference_value)
        return  true;
    else
    {
        cout << "FAILED: x: " << to_string(x) << "; " <<
                          "y: "  << to_string(y) << "; " <<
                          "expected: " << to_string(reference_value) << "; " <<
                          "received: " << to_string(calculated) << endl;
        return false;
    }

}


