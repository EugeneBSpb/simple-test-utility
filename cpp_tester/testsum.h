#ifndef EXAMPLETEST_H
#define EXAMPLETEST_H

#include "../common_tester_files/tester.h"




class SumTest : public TestObject
{
public:
    SumTest(const std::string &test_name);
    virtual bool test() const override;
    bool singleTest(int x, int y, int reference_value) const;
};

#endif // EXAMPLETEST_H
