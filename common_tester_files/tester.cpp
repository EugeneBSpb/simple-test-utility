#include "tester.h"
#include <cstddef>
#include <iostream>

Tester::Tester()
{

}


bool Tester::performTests()
{
    using namespace std;
    bool result = true;
    vector<unique_ptr<const TestObject>>::iterator it;
    for(it = m_testObjects.begin(); it != m_testObjects.end(); ++it)
    {
        cout << "Test " << (*it)->getName() << " started...\n";
        if((*it)->test() == true)
        {
            cout << "Test " << (*it)->getName() << " Ok" << "\n";
        }
        else
        {
            result = false;
            cout << "Test " << (*it)->getName() << " FAILED"  << "\n\n";
            break;
        }
        cout << "Test " << (*it)->getName() << " finished\n\n";
    }

    if(result)
        cout << "Tests Ok\n";
    else
        cout << "Tests FAILED\n";

    return result;
}

bool Tester::addTestObject(std::unique_ptr<const TestObject> new_test)
{
    if(new_test != nullptr)
    {
        m_testObjects.push_back(move(new_test));
        return true;
    }
    else
    {
        return false;
    }
}

