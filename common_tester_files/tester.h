/**
 * Description:  Simple utility for testing functionality of a custom c++ class.
 *
 *
 * Usage:
 * 1. Inherit a custom class from the TestObject. Implement the test() method.
 * 2. Declare the inherited class as a friend of the class you want to test.
 * 3. Declare a variable of the type Tester in any other source (main.cpp for example).
 * 4. Declare a variable of your custom test and enqueue it with the method Tester::addTestObject(...)
 * 5. Run Tester::performTests() method. If any test fail, the execution will be interrupted.
 *
 *
 *
 **/


#ifndef TESTER_H
#define TESTER_H

#include <vector>
#include <string>
#include <memory>


class TestObject
{
public:
    TestObject(const std::string &test_name) {m_testName = test_name;}
    virtual ~TestObject(){}
    virtual bool test() const = 0;
    std::string getName() const {return m_testName;}
protected:
    std::string m_testName;
};


class Tester
{
public:
    Tester();
    bool performTests();
    bool addTestObject(std::unique_ptr<const TestObject> new_test);

private:
    std::vector<std::unique_ptr<const TestObject>> m_testObjects;
};



#endif // TESTER_H
